/**
 *
 * @author Mind_Dreamer
 */
public class ArrayQueueSingleton {

    private static int top = 0;
    private static Object object[] = new Object[5];

    public static boolean add(Object ob) {
        assert ob != null;
        try {
            object[top++] = ob;
        } catch (IllegalStateException e) {
            System.out.println("No available space!");
            return false;
        }
        return true;
    }

    public static Object peek() {
        if (top == 0) {
            System.out.println("Queue is empty");
            return null;
        }
        return object[object.length - 1 - top--];
    }

    public static boolean isEmpty() {
        return top == 0;
    }
}
